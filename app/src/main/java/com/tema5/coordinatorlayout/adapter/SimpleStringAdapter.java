package com.tema5.coordinatorlayout.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema5.coordinatorlayout.Cheeses;
import com.tema5.coordinatorlayout.R;

import java.util.List;

/**
 * Created by Sergio on 11/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class SimpleStringAdapter extends RecyclerView.Adapter<SimpleStringAdapter.ViewHolder> {

    private final int mBackground;
    private final List<String> mValues;

    public SimpleStringAdapter(Context context, List<String> items) {
        TypedValue mTypedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        mValues = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fab_list_item, parent, false);
        view.setBackgroundResource(mBackground);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mBoundString = mValues.get(position);
        holder.mTextView.setText(mValues.get(position));

        //holder.mView.setOnClickListener(new View.OnClickListener() {
        //                @Override
        //                public void onClick(View v) {
        //                    Context context = v.getContext();
        //                    Intent intent = new Intent(context, CheeseDetailActivity.class);
        //                    intent.putExtra(CheeseDetailActivity.EXTRA_NAME, holder.mBoundString);
        //
        //                    context.startActivity(intent);
        //                }
        //            });

        holder.mImageView.setImageResource(new Cheeses().getRandomCheesesDrawable());
//        RequestOptions options = new RequestOptions();
//        Glide.with(holder.mImageView.getContext())
//                .load(new Cheeses().getRandomCheesesDrawable())
//                .apply(options.fitCenter())
//                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mImageView;
        final TextView mTextView;
        String mBoundString;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.avatar);
            mTextView = view.findViewById(android.R.id.text1);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mTextView.getText();
        }
    }
}
