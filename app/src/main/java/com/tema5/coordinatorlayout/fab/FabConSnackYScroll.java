package com.tema5.coordinatorlayout.fab;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.tema5.coordinatorlayout.Cheeses;
import com.tema5.coordinatorlayout.R;
import com.tema5.coordinatorlayout.adapter.SimpleStringAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FabConSnackYScroll extends AppCompatActivity {

    private FloatingActionButton fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_y_snackbar);

        RecyclerView rv = findViewById(R.id.recyclerview);
        setupRecyclerView(rv);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("FAB y SnackBar");

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // El Snackbar es parecido a un toast. Se muestra un mensaje corto en la parte inferior de la pantalla
                // y permite mostrar un botón que realiza una acción
                Snackbar.make(view, "Esto es un SnackBar", Snackbar.LENGTH_LONG)
                        .setAction("Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(FabConSnackYScroll.this, "Se pulsó el SnackBar", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
            }
        });

        // listener que detecta cuando se hace scroll en un recyclerview
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });
    }

    /**
     * Método para configurar el recyclerview
     *
     * @param recyclerView -> recibe como parámetro el recyclerview a configurar
     */
    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new SimpleStringAdapter(FabConSnackYScroll.this,
                getRandomSublist(new Cheeses().getCheeseString(), 30)));
    }

    /**
     * Método para obtener el listado random de quesos
     *
     * @param array  -> array con todos los quesos
     * @param amount -> cantidad de quesos a mostrar
     * @return -> devuelve un listado de quesos
     */
    private List<String> getRandomSublist(String[] array, int amount) {
        ArrayList<String> list = new ArrayList<>(amount);
        Random random = new Random();
        while (list.size() < amount) {
            list.add(array[random.nextInt(array.length)]);
        }
        return list;
    }

}
