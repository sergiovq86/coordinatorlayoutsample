package com.tema5.coordinatorlayout.toolbar;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tema5.coordinatorlayout.Cheeses;
import com.tema5.coordinatorlayout.R;
import com.tema5.coordinatorlayout.adapter.SimpleStringAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ToolbarScroll extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar_scroll);
        RecyclerView rv = findViewById(R.id.recyclerview);
        setupRecyclerView(rv);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Toolbar scroll");

    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new SimpleStringAdapter(ToolbarScroll.this,
                getRandomSublist(new Cheeses().getCheeseString(), 30)));
    }

    private List<String> getRandomSublist(String[] array, int amount) {
        ArrayList<String> list = new ArrayList<>(amount);
        Random random = new Random();
        while (list.size() < amount) {
            list.add(array[random.nextInt(array.length)]);
        }
        return list;
    }

}
