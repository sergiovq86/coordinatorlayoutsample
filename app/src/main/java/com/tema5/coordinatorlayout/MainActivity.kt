package com.tema5.coordinatorlayout

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.tema5.coordinatorlayout.bottomappbar.BottomAppBarActivity
import com.tema5.coordinatorlayout.collapsingfab.CollapseYFab
import com.tema5.coordinatorlayout.collapsingtoolbar.CollapseCase1
import com.tema5.coordinatorlayout.collapsingtoolbar.CollapseCase2
import com.tema5.coordinatorlayout.collapsingtoolbar.CollapseCase3
import com.tema5.coordinatorlayout.collapsingtoolbar.CollapseCase4
import com.tema5.coordinatorlayout.fab.FabConSnackYScroll
import com.tema5.coordinatorlayout.fab.FabEnCabecera
import com.tema5.coordinatorlayout.parallax.ParallaxEfect
import com.tema5.coordinatorlayout.tablayout.ScrollTabLayout
import com.tema5.coordinatorlayout.toolbar.ToolbarEnterAlways
import com.tema5.coordinatorlayout.toolbar.ToolbarScroll

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", View.OnClickListener { view2 ->
                    Toast.makeText(this, "Pulsado!!", Toast.LENGTH_LONG).show()
                }).show()
        }

        val button: Button = findViewById(R.id.button1)
        button.setOnClickListener { view ->
            fabAlertDialog()
        }

        findViewById<Button>(R.id.button3).setOnClickListener { view ->
            openActivity(BottomAppBarActivity::class.java)
        }

        findViewById<Button>(R.id.button4).setOnClickListener { view ->
            toolbarAlertDialog()
        }

        findViewById<Button>(R.id.button6).setOnClickListener { view ->
            colllapsingAlertDialog()
        }

        findViewById<Button>(R.id.button10).setOnClickListener { view ->
            openActivity(ScrollTabLayout::class.java)
        }

        findViewById<Button>(R.id.button11).setOnClickListener { view ->
            openActivity(CollapseYFab::class.java)
        }

        findViewById<Button>(R.id.button12).setOnClickListener { view ->
            openActivity(ParallaxEfect::class.java)
        }
    }

    fun fabAlertDialog() {
        val lista = arrayOf("FAB hiding/showing con SnackBar", "FAB anclado entre vistas")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista, DialogInterface.OnClickListener { dialog, which ->
            if (which == 0) {
                openActivity(FabConSnackYScroll::class.java)
            } else {
                openActivity(FabEnCabecera::class.java)
            }
        })

        builder.create().show()
    }

    fun toolbarAlertDialog() {
        val lista = arrayOf("Toolbar son scroll", "Toolbar con snterAlways")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista, DialogInterface.OnClickListener { dialog, which ->
            if (which == 0) {
                openActivity(ToolbarScroll::class.java)
            } else {
                openActivity(ToolbarEnterAlways::class.java)
            }
        })

        builder.create().show()
    }

    fun colllapsingAlertDialog() {
        val lista = arrayOf(
            "Collapsing scroll|enterAlways",
            "Collapsing scroll|enterAlwaysCollapsed",
            "Collapsing scroll|enterAlways|enterAlwaysCollapsed",
            "Collapsing scroll|exitUntilCollapsed"
        )

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista, DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                0 -> openActivity(CollapseCase1::class.java)
                1 -> openActivity(CollapseCase2::class.java)
                2 -> openActivity(CollapseCase3::class.java)
                3 -> openActivity(CollapseCase4::class.java)
            }
        })

        builder.create().show()
    }

    fun openActivity(clase: Class<*>) {
        startActivity(Intent(this, clase))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}