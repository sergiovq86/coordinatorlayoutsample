package com.tema5.coordinatorlayout.tablayout;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;
import com.tema5.coordinatorlayout.R;

public class ScrollTabLayout extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablayout_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Toolbar scroll");

        TabLayout tabLayout = findViewById(R.id.tablayout);
        navigateToFragment(0);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                navigateToFragment(id);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void navigateToFragment(int itemId) {
        //fragmen y titulo por defecto
        Fragment fragment;
        // getString es un método de la clase Activity para obtener textos de res/values/strings.xml

        // según la id del item pulsado, creamos ese fragment, y cambiamos el titulo
        switch (itemId) {
            default:
                fragment = new Tab1Fragment();
                break;

            case 1:
                fragment = new Tab2Fragment();
                break;

            case 2:
                fragment = new Tab3Fragment();
                break;
        }

        // transacción del fragment al framelayout
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
        transaction.commit();
    }

}
