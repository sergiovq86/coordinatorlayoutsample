package com.tema5.coordinatorlayout.parallax;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.tema5.coordinatorlayout.R;


public class ParallaxEfect extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parallax);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Toolbar enteralways");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


}
